#- Loading of all E3 modules and versions
require busy,1.7.2_c596e1c
require autosave,5.10.0
require asyn,4.37.0
require ADCore,3.9.0
require admisc,2.1.0
require iocStats,3.1.16
require recsync,1.3.0-9705e52
require mrfioc2,2.2.1rc1
require adsis8300,devel

#- load the adsis8300 setup commands
iocshLoad("./adsis8300.iocsh")

#- load the autosave module
iocshLoad("./adsis8300-autosave.iocsh")

#- run IOC
iocInit

